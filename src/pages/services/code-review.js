import React from "react"
import Layout from "../../components/layout";
import { Row, Col, Image } from "react-bootstrap";
import imgReview from "../../images/review-large.png";
import SEO from "../../components/seo";

export default function CodeReview() {
  return (
    <Layout>
      <SEO title="Code Review" />
      <Row>
            <Col md={5}>
                <Image src={imgReview} alt="Code Review" fluid className="mx-auto d-block" />                
            </Col>
            <Col md={7}>
                <h1>Code Review</h1>
                <p className="lead">
                  Acquire the potential returns from your software with our in-depth review of your code and expert proposals for improvement.
                </p>
                <h3>Understand Your Code Quality</h3>
                <p className="lead">
                  Our suggestions and proposals for long-term improvement, offered by our full code analysis enables you to optimize your systems to be as secured, fast, extensible, maintainable, and bug-free as possible, which could offer significant cost savings over time.
                </p>
                <p className="lead">
                  Complete technical assessments of your codes and its environment combines a range of techniques that assess the scale and complexity to benchmark how well it performs in every part critical to business continuity such as:
                  <ul>
                      <li>Security</li>
                      <li>Performance</li>
                      <li>Extensibility</li>
                      <li>Maintainability</li>                
                  </ul>
                </p>
            </Col>
        </Row>
    </Layout> 
  );
}
