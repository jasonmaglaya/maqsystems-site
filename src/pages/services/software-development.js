import React from "react"
import Layout from "../../components/layout";
import { Row, Col, Image } from "react-bootstrap";
import imgDevelopment from "../../images/development-large.png";
import SEO from "../../components/seo";

export default function SoftwareDevelopment() {
  return (
    <Layout>
      <SEO title="Software Development" />
      <Row>
            <Col md={5}>
                <Image src={imgDevelopment} alt="Software Development" fluid className="mx-auto d-block" />                
            </Col>
            <Col md={7}>
                <h1>Software Development</h1>
                <p className="lead">
                  Build software with our expert software designers, architects and engineers.
                </p>
                <h3>Better Software Through a Better Approach</h3>
                <p className="lead">
                  We provide our clients with perceptibly value added software delivery and technology development services. These include providing a unique combination of approaches.
                  <ul>
                      <li>Project management</li>
                      <li>System architecture and design</li>
                      <li>Custom software development</li>
                      <li>A/B testing</li>
                  </ul>
                </p>
            </Col>
        </Row>
    </Layout> 
  );
}
