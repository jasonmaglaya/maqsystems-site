import React from "react"
import Layout from "../../components/layout";
import { Row, Col, Image } from "react-bootstrap";
import imgDesign from "../../images/design-large.png";
import SEO from "../../components/seo";

export default function SoftwareDesign() {
  return (
    <Layout>
        <SEO title="Software Design" />
        <Row>
            <Col md={5}>
                <Image src={imgDesign} alt="Software Design" fluid className="mx-auto d-block" />                
            </Col>
            <Col md={7}>
                <h1>Software Design</h1>
                <p className="lead">
                    Transform business requirements into a well-researched technical specification and design that provides a fundamental guiding reference for development.
                </p>
                <h3>Invest in Software Design</h3>
                <p className="lead">
                    Amplify accuracy and speed in software development by providing with a clear outline of reference throughout the project development. We deliver technical specifications, crafted by our expert software architects and engineers that ensure all parts of your software are well-defined before development commences to avoid pointless project risk, difficulty and cost and allow you to develop something far and more advanced product.
                </p>
            </Col>
        </Row>
    </Layout> 
  );
}
