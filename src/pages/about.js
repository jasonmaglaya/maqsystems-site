import React from "react"
import Layout from "../components/layout";
import SEO from "../components/seo";

export default function About() {
  return (
    <Layout>
      <SEO title="About Us" />
      <h1>About Us</h1>
      <h2>We creatively build your systems</h2>
      <p className="lead">As professional software developers, you can count on our capacities to create real value for your business. We commit to take on innovations without sacrificing the endless evolution of your systems.</p>
    </Layout> 
  );
}
