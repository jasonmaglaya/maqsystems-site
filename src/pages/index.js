import React from "react"
import Layout from "../components/layout";
import { Carousel, Image, Row, Col } from "react-bootstrap";
import { Link } from "gatsby";
import imgSlide1 from "../images/slide-01.jpg";
import imgSlide2 from "../images/slide-02.jpg";
import imgSlide3 from "../images/slide-03.jpg";
import imgDesign from "../images/design.png";
import imgDevelopment from "../images/development.png";
import imgReview from "../images/review.png";
import BladeSmsBanner from "../components/blade-sms-banner";
import SEO from "../components/seo";

export default function Home() {
  return (
    <Layout>
      <SEO title="Home" />
      <Carousel>
        <Carousel.Item>
          <Image src={imgSlide1} />
          <Carousel.Caption>
            <h3>Software Design</h3>
            <p>Transform business requirements into a well-researched technical specification and design that provides a fundamental guiding reference for development.</p>
            <p><Link to="/services/software-design" className="btn btn-primary">Learn more »</Link></p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <Image src={imgSlide2} />
          <Carousel.Caption>
            <h3>Software Development</h3>
            <p>Build software with our expert software designers, architects and engineers.</p>
            <p><Link to="/services/software-development" className="btn btn-primary">Learn more »</Link></p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <Image src={imgSlide3} />
          <Carousel.Caption>
            <h3>Code Review</h3>
            <p>Acquire the potential returns from your software with our in-depth review of your code and expert proposals for improvement.</p>
            <p><Link to="/services/code-review" className="btn btn-primary">Learn more »</Link></p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
      <BladeSmsBanner />
      <Row>
        <Col md={4}>
          <Link to="/services/software-design">
            <Image src={imgDesign} alt="Software Design" fluid className="mx-auto d-block" />
          </Link>
          <h2 className="text-center">Software Design</h2>
          <p>Transform business requirements into a well-researched technical specification and design that provides a fundamental guiding reference for development.</p>
          <p><Link to="/services/software-design" className="btn btn-primary">Learn more »</Link></p>
        </Col>
        <Col md={4}>
          <Link to="/services/software-development">
            <Image src={imgDevelopment} alt="Software Development" fluid className="mx-auto d-block"/>
          </Link>
          <h2 className="text-center">Software Development</h2>
          <p>Build software with our expert software designers, architects and engineers.</p>
          <p><Link to="/services/software-development" className="btn btn-primary">Learn more »</Link></p>
        </Col>
        <Col md={4}>
          <Link to="/services/code-review">
            <Image src={imgReview} alt="Code Review" fluid className="mx-auto d-block"/>
          </Link>
          <h2 className="text-center">Code Review</h2>
          <p>Acquire the potential returns from your software with our in-depth review of your code and expert proposals for improvement.</p>
          <p><Link to="/services/code-review" className="btn btn-primary">Learn more »</Link></p>
        </Col>
      </Row>
    </Layout> 
  );
}
