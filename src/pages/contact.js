import React from "react"
import Layout from "../components/layout";
import SEO from "../components/seo";

export default function ContactUs() {
  return (
    <Layout>
      <SEO title="Contact Us" />
      <h1>Contact Us</h1>
      <h2>MAQ Systems Inc.</h2>
      <address>
          B13 L6779 Cruzero St.<br />
          Villa Carolina, Tunasan, Muntinlupa<br />
          Philippines 1773 <br />
          <abbr title="Mobile">M:</abbr>
          +63 939 9352766
      </address>
      <address>
          <strong>Inquiry:</strong>   <a href="mailto:info@maqsystems.com">info@maqsystems.com</a><br />
          <strong>Sales:</strong> <a href="mailto:sales@maqsystems.com">sales@maqsystems.com</a><br />
          <strong>Support:</strong>   <a href="mailto:support@maqsystems.com">support@maqsystems.com</a>
      </address>
    </Layout> 
  );
}
