import React from "react"
import Layout from "../../components/layout";
import { Row, Col, Image } from "react-bootstrap";
import { Link } from "gatsby";
import imgHub from "../../images/hub.png";
import SEO from "../../components/seo";

export default function BladeSms() {
  return (
    <Layout>
        <SEO title="Blade SMS" />
        <Row>
            <Col md={5}>
                <Image src={imgHub} alt="Blade SMS" fluid className="mx-auto d-block" />                
            </Col>
            <Col md={7}>
                <h1>Blade SMS</h1>
                <p>Blade is a high performance messaging platform that offers secured enterprise-grade SMS solution. It has a set of professional applications, as well as a powerful collection of APIs for any type of integration. It allows companies to automate their SMS notifications delivery. </p>
                <p>With mobile messaging, you can improve efficiency, introduce a new way of building customer loyalty and transform the way your clients experience and interact with your business. Give your business a more powerful voice, go mobile and go global.</p>
                <p>Connecting to Blade means connecting to 800+ operators in 190+ countries on all continents. To put it short, <em>the World.</em></p>
                <p>
                    <h3>Fully featured Blade SMS Solution</h3>
                    <ul>
                        <li>High message throughput, no queues</li>
                        <li>Direct connections to mobile operators</li>
                        <li>Real-time delivery reports</li>
                        <li>Powerful management and SMS messaging web portals</li>
                        <li>HTTP and industry standard SMPP v3.4 APIs</li>
                        <li>Unicode, binary and multi-part (concatenated) messages</li>
                    </ul>
                </p>
                <p><Link to="/contact" className="btn btn-primary">Contact Us</Link></p>
            </Col>
        </Row>
    </Layout> 
  );
}
