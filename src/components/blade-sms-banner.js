import React from "react";
import { Row, Col, Image } from "react-bootstrap";
import { Link } from "gatsby";
import styles from "./blade-sms-banner.module.css";
import imgBladeSms from "../images/blade.png";

export default function BladeSmsBanner() {
    return (
        <Row className={styles.blade}>
            <Col md={4}>
                <Image src={imgBladeSms} fluid className="mx-auto d-block" />
            </Col>
            <Col md={8}>
                <h1>Blade SMS</h1>
                <p className="lead">Send notifications, advertise and alert with our fully featured Blade SMS, anytime and anywhere.</p>
                <Link to="/products/blade-sms" className="btn btn-primary">Learn more »</Link>
            </Col>
        </Row>
    );
}