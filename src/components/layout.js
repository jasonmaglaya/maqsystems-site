import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Header from "./header";

export default function Layout(props) {
    const { children } = props;
    return (
        <>
        <Header />
        <div className="container">
            {children}
            <hr />
            <footer>
                <p>&copy; {new Date().getFullYear()} MAQ Systems Inc. All Rights Reserved</p>
            </footer>
        </div>
        </>
    );
}