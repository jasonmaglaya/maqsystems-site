import React from "react"
import { Navbar, Nav, NavDropdown, Image } from "react-bootstrap";
import { Link } from "gatsby";
import imgMaq from "../images/maq.png";

export default function Header() {
    return (
        <Navbar expand="lg">
            <Link to="/" className="navbar-brand"><Image src={imgMaq} alt="MAQ Systems Inc." /></Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Link to="/" className="nav-link">Home</Link>
                    <NavDropdown title="Services" id="basic-nav-dropdown">
                        <Link to="/services/software-design" className="nav-link">Software Design</Link>
                        <Link to="/services/software-development" className="nav-link">Software Development</Link>
                        <Link to="/services/code-review" className="nav-link">Code Review</Link>
                    </NavDropdown>
                    <NavDropdown title="Products" id="basic-nav-dropdown">
                        <Link to="/products/blade-sms" className="nav-link">Blade SMS</Link>
                    </NavDropdown>
                    <Link to="/about" className="nav-link">About Us</Link>
                    <Link to="/contact" className="nav-link">Contact Us</Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}