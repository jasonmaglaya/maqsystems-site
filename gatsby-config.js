/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  siteMetadata: {
    title: `MAQ Systems Inc.`,
    description: `We Design and Build Custom Software for Business`,
    author: `MAQ Systems Inc.`,
  },
  plugins: [],
}
